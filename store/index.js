import axios from 'axios'

const API_KEY = "9de243494c0b295cca9337e1e96b00e2";
const SERVICE_URL = "https://api.openweathermap.org/data/2.5";

export const state = () => ({
  todos: [],
  // used as an unique id for each todo
  todoNumber: 0
});

export const mutations = {
  SET_TODOS: function (state, newTodos) {
    state.todos = newTodos;
  },
  INCREMENT_TODONUMBER: function (state) {
    state.todoNumber++;
  }
};

export const actions = {
  async createTodo ({commit, state}, newTodoText) {
    commit('INCREMENT_TODONUMBER');
    let weather = {};
    try {
      weather = await axios.get(`${SERVICE_URL}/weather?q=${newTodoText}&units=metric&appid=${API_KEY}`)
    }catch(e){
      console.log(e)
      alert('Немає такого міста, введіть наприклад: Київ')
    }
    if(weather.data !== undefined){
    const newTodo = {
      id: state.todoNumber,
      text: `${newTodoText} Температура: ${weather.data.main.temp} Відчувається як: ${weather.data.main.feels_like}`,
      isDone: false
    }
    const oldTodos = state.todos;
    const newTodos = [newTodo, ...oldTodos]
    commit('SET_TODOS', newTodos);
   }
  },
  async updateTodo ({commit, state}, {todoIdtoUpdate, propertiesToUpdate}) {
    const oldTodos = state.todos;
    let weather = {};
    console.log('propertiesToUpdate',propertiesToUpdate)
    let newTodoText = propertiesToUpdate.text.split(' ')[0]
    try {
      weather = await axios.get(`${SERVICE_URL}/weather?q=${newTodoText}&units=metric&appid=${API_KEY}`)
    }catch(e){
      console.log(e)
      alert('Немає такого міста, введіть наприклад: Київ')
    }
    let withWeatherUpdate = {text:`${newTodoText} Температура: ${weather.data.main.temp} Відчувається як: ${weather.data.main.feels_like} UPD`};
    const newTodos = oldTodos.map(todo => {
      if (todo.id === todoIdtoUpdate) {
        return {...todo, ...withWeatherUpdate}
      } else {
        return todo;
      }
    });
    commit('SET_TODOS', newTodos);
  },
  async deleteTodo ({commit, state}, todoIdtoDelete) {
    const oldTodos = state.todos;
    const newTodos = oldTodos.filter(todo => todo.id !== todoIdtoDelete);
    commit('SET_TODOS', newTodos);
  },
  async updateTodos ({commit}, newTodos) {
    commit('SET_TODOS', newTodos);
  }
};
